#!/usr/bin/env sh

# TODO: How to set minimum coverage threshold for branch coverage AND line coverage?
dotnet test LinqQuerySpecification.sln /p:CollectCoverage=true /p:Exclude=\"[xunit.*]*,[testUtilities*]*\" /p:IncludeTestAssembly=false /p:CoverletOutputFormat=cobertura /p:CoverletOutput=TestResults/Coverage/ /p:ThresholdType=line /p:Threshold=51
