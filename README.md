# LinqQuerySpecification Project

This is a C#.net library that implements the Query Specification pattern for use in other C# projects. It is modeled around the Java implementation of the [jHipster](http://www.jhipster.tech) library and useful for [implementing filtering logic](https://www.jhipster.tech/entities-filtering) in a MVC-based application.

For information related to contrbuting to the project, [see here](Contributing.md)

## Example usage

The following code examples are written with the `License` entity as the example. All the classes follow normal MVC naming conventions.

```cs
  # License.cs
  public class License
  {
    public long Id { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }

    public bool IsActive { get; set; } = true;

    public DateTime CreatedAt { get; set; }
  }
```

```cs
  # LicenseDbContext.cs
  public class LicenseDbContext : DbContext
  {
    public LicenseDbContext(DbContextOptions<LicenseDbContext> options)
      : base(options)
    {
    }

    public LicenseDbContext()
    {
    }

    public virtual IEnumerable<License> Licenses { get; }
  }
```

```cs
  # LicenseService.cs
  public class LicenseService
  {
    private readonly LicenseDbContext context;

    public LicenseService(LicenseDbContext context) => this.context = context;

    public IEnumerable FindAll(LicenseCriteria criteria)
    {
      var licenses = context.Licenses.AsQueryable();
      if (criteria != null)
      {
        licenses = criteria.AddWhere(licenses);
      }

      return licenses;
    }
  }
```

```cs
  # LicenseCriteria.cs
  public class LicenseCriteria
  {
    private LongFilter Id { get; set; }

    private StringFilter Name { get; set; }

    private StringFilter Description { get; set; }

    private BooleanFilter IsActive { get; set; }

    private DateFilter CreatedAt { get; set; }

    public IQueryable<License> AddWhere(IQueryable<License> licenses)
    {
      CheckNotNull(licenses, nameof(licenses));

      licenses = Id == null ? licenses : Id.Apply(licenses, nameof(License.Id));
      licenses = Name == null ? licenses : Name.Apply(licenses, nameof(License.Name));
      licenses = Description == null ? licenses : Description.Apply(licenses, nameof(License.Description));
      licenses = IsActive == null ? licenses : IsActive.Apply(licenses, nameof(License.IsActive));
      licenses = CreatedAt == null ? licenses : CreatedAt.Apply(licenses, nameof(License.CreatedAt));

      return licenses;
    }

    private static void CheckNotNull(IQueryable queryable, string name)
    {
      if (queryable == null)
      {
        throw new ArgumentNullException($"{name} is null");
      }
    }
  }
```

```cs
  # LicensesController.cs
  // GET api/licenses
  [HttpGet]
  public ActionResult<List<License>> Index([FromQuery] LicenseCriteria criteria)
  {
    var licenses = LicenseService.FindAll(criteria);
    return Ok(licenses);
  }
```
