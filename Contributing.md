# Contributing to this codebase

## Setup Pre-requisites

1. Install the following:

    - [.NET Core 3.1](https://www.microsoft.com/net/core)

## Building the code

To build the solution, run the following command:

```bash
  ./build-dotnet.sh
```

## Running the tests

To run all the unit tests, run this command:

```bash
  ./test-dotnet.sh
```

This will run all the tests and publish the test coverage reports to the following directory:

```bash
  TestResults/Coverage/Reports/index.html
```

## Formatting the code

To format the dotnet code, run the following command:

```bash
  ./format-dotnet.sh
```

## Publish nuget package

Compiling the source will create the nuget package. After this, to publish make sure you have the api-key from nuget.org, then run

```bash
  cd linq-query-specification/api/bin/Debug/ && dotnet nuget push Linq.Query.Filters.0.1.0.nupkg -k <api-key> -s https://api.nuget.org/v3/index.json
```

## Contributors (in alphabetical order)

Saravanakumar Kumarasamy (@saravananbscitm)
Sriram Mani (@sriram.mani)
Vijay Raghavan Aravamudhan (@avijayr)
