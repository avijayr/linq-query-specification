namespace LinqQuerySpecification.Tests.Filters
{
  using System.Collections.Generic;
  using System.Linq;
  using LinqQuerySpecification.Filters;
  using Xunit;

  public class BooleanFilterTests
  {
    private class ClassToTestBool
    {
      public bool? IsEnabled { get; set; }
    }

    [Fact]
    public void ShouldReturnValuesWithEqualToFilter()
    {
      var testData = GetTestData();

      var result = new BooleanFilter { EqualTo = true }
        .Apply(testData.AsQueryable(), nameof(ClassToTestBool.IsEnabled));

      Assert.Equal(2, result.Count());
      Assert.Contains(result.AsEnumerable(), x => x == testData[0] || x == testData[2]);
      Assert.DoesNotContain(result.AsEnumerable(), x => x == testData[1]);
    }

    [Fact]
    public void ShouldReturnValuesWithNotEqualToFilter()
    {
      var testData = GetTestData();

      var result = new BooleanFilter { NotEqualTo = true }
        .Apply(testData.AsQueryable(), nameof(ClassToTestBool.IsEnabled));

      Assert.Equal(1, result.Count());
      Assert.DoesNotContain(result.AsEnumerable(), x => x == testData[0] || x == testData[2]);
      Assert.Contains(result.AsEnumerable(), x => x == testData[1]);
    }

    [Fact]
    public void ShouldReturnValuesWithSpecifiedFilter()
    {
      var testData = new List<ClassToTestBool> { new ClassToTestBool { IsEnabled = true }, new ClassToTestBool() };

      var result = new BooleanFilter { Specified = false }
        .Apply(testData.AsQueryable(), nameof(ClassToTestBool.IsEnabled));

      Assert.Equal(1, result.Count());
      Assert.DoesNotContain(result.AsEnumerable(), x => x == testData[0]);
      Assert.Contains(result.AsEnumerable(), x => x == testData[1]);
    }

    [Fact(Skip = "TODO: Not working as expected")]
    public void ShouldReturnValuesWithInFilter()
    {
      var testData = GetTestData();

      var result = new BooleanFilter { In = new[] { (bool?)true } }
        .Apply(testData.AsQueryable(), nameof(ClassToTestBool.IsEnabled));

      Assert.Equal(1, result.Count());
    }

    private static List<ClassToTestBool> GetTestData() =>
      new List<ClassToTestBool>
      {
        new ClassToTestBool { IsEnabled = true },
        new ClassToTestBool { IsEnabled = false },
        new ClassToTestBool { IsEnabled = true },
      };
  }
}
