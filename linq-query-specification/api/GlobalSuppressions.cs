// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Style", "IDE0065: Misplaced using directive", Justification = "Following Microsoft suggested coding standard by keeping Using statements within namespace", Scope = "module")]
[assembly: SuppressMessage("Style", "SA0001: XML comment analysis is disabled due to project configuration", Justification = "XML Comment is skipped", Scope = "module")]
[assembly: SuppressMessage("Style", "SA1101: Prefix local calls with this", Justification = "Skipping explicit usage of 'this' keyword", Scope = "module")]
[assembly: SuppressMessage("Style", "SA1309: Field '_context' should not begin with an underscore", Justification = "Using underscore for private variables", Scope = "module")]
[assembly: SuppressMessage("Style", "SA1633: The file header is missing or not located at the top of the file.", Justification = "Skipping to add file comments", Scope = "module")]
