namespace LinqQuerySpecification.Filters
{
  /// <summary>
  ///   Class for filtering attributes with <see cref="long" /> type.
  /// </summary>
  /// <remarks>
  ///   It can be added to a criteria class as a member, to support the following query parameters:
  ///   <code>
  ///    fieldName.EqualTo = 42
  ///    fieldName.NotEqualTo = 42
  ///    fieldName.Specified = true  // or false
  ///    fieldName.In = 43, 42
  ///    fieldName.GreaterThan = 41
  ///    fieldName.GreaterThanOrEqual = 42
  ///    fieldName.LessThan = 44
  ///    fieldName.LessThanOrEqual = 44
  ///  </code>
  /// </remarks>
  public class LongFilter : RangeFilter<long?>
  {
  }
}
