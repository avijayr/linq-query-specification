namespace LinqQuerySpecification.Filters
{
  /// <summary>
  ///   Class for filtering attributes with <see cref="bool" /> type.
  /// </summary>
  /// <remarks>
  ///   It can be added to a criteria class as a member, to support the following query parameters:
  ///   <code>
  ///    fieldName.EqualTo = true
  ///    fieldName.NotEqualTo = false
  ///    fieldName.Specified = true  // or false
  ///    fieldName.In = true, false
  ///  </code>
  /// </remarks>
  public class BooleanFilter : Filter<bool?>
  {
  }
}
